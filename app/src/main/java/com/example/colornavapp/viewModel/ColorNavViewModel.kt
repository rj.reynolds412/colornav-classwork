package com.example.colornavapp.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.colornavapp.model.ColorRepo
import kotlinx.coroutines.launch

class ColorNavViewModel : ViewModel() {
    private val repo = ColorRepo

    private val _randomColor = MutableLiveData<Int>()
    val color: LiveData<Int> get() = _randomColor

    fun getRandomColor() {
        viewModelScope.launch {
            val randomColor = repo.getRandomColor()
            _randomColor.value = randomColor
        }
    }
}