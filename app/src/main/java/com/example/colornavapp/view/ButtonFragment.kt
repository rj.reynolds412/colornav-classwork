package com.example.colornavapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.colornavapp.databinding.FragmentButtonBinding
import com.example.colornavapp.model.randomColor
import com.example.colornavapp.viewModel.ColorNavViewModel

class ButtonFragment : Fragment() {
    private var _binding: FragmentButtonBinding? = null
    private val binding get() = _binding!!
    private val colorNavViewModel by viewModels<ColorNavViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentButtonBinding.inflate(inflater, container, false
    ).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnGetColor.setOnClickListener {
            colorNavViewModel.getRandomColor()
        }
        colorNavViewModel.color.observe(viewLifecycleOwner) { randomColor ->
            val nav = ButtonFragmentDirections.actionButtonFragmentToColorFragment(randomColor)
            findNavController().navigate(nav)
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}