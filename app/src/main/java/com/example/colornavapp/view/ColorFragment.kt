package com.example.colornavapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController
import com.example.colornavapp.R
import com.example.colornavapp.databinding.FragmentButtonBinding
import com.example.colornavapp.databinding.FragmentColorBinding
import com.example.colornavapp.model.randomColor

class ColorFragment : Fragment() {
    private var _binding: FragmentColorBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ) = FragmentColorBinding.inflate(inflater, container, false
    ).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let { binding.colorView.setBackgroundColor(randomColor) }
        Toast.makeText(this.context, getString(R.string.congrats_toast), Toast.LENGTH_SHORT).show()
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner)
        { findNavController().navigate(ColorFragmentDirections.actionColorFragmentToButtonFragment()) }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}