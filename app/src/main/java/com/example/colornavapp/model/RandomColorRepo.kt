package com.example.colornavapp.model

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext


object ColorRepo {
    private val colorApi = object : ColorApi {
        override suspend fun getRandomColor(): Int {
            return randomColor
        }

    }

    suspend fun getRandomColor(): Int = withContext(Dispatchers.IO) {
        delay(5000L)
        colorApi.getRandomColor()
    }
}