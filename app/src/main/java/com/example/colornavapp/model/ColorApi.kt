package com.example.colornavapp.model

interface ColorApi {
    suspend fun getRandomColor(): Int

}
